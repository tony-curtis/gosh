package main

import (
    "fmt"
    "bitbucket.org/tony-curtis/go-shmem" // OpenSHMEM namespace
)

// symmetric
var dest int = 9

func main() {
    shmem.Init()

    me := shmem.MyPe()

    var new int = -666

    if me == 0 {
        new = shmem.IntAtomicFetchInc(&dest, 1)
    }

    shmem.BarrierAll()

    fmt.Printf("%-4d dest = %-4d, new = %-4d\n", me, dest, new)

    shmem.Finalize()
}
