package main

import (
    "fmt"
    "math"
    "math/rand"
    "strconv"
    "os"
    "bitbucket.org/tony-curtis/go-shmem" // OpenSHMEM namespace
)

var pSyncR [shmem.REDUCE_SYNC_SIZE]int64 /* for reduction */
var pWrkR  [shmem.REDUCE_MIN_WRKDATA_SIZE]int32 /* for reduction */

func make_random_value() float64 {
    return rand.Float64()
}

func buffon_laplace_simulate(a float64, b float64, l float64, trial_num int32) int32 {
    tn := int(trial_num)
    var hits int32 = 0

    for t := 1; t <= tn; t++ {
        //
        // Randomly choose the location of the eye of the needle in
        // [0,0]x[A,B],
        // and the angle the needle makes.
        //
        x1 := a * make_random_value()
        y1 := b * make_random_value()
        angle := 2.0 * math.Pi * make_random_value()
        //
        // Compute the location of the point of the needle.
        //
        x2 := x1 + l * math.Cos(angle)
        y2 := y1 + l * math.Sin(angle)
        //
        // Count the end locations that lie outside the cell.
        //
        if x2 <= 0.0 || a <= x2 || y2 <= 0.0 || b <= y2 {
            hits++
        }
    }

    return hits
}

func r8_huge() float64 {
    return 1.0e+30
}

var hit_total int32 = 0
var hit_num int32 = 0

func main() {

    var trial_num = 100000

    if len(os.Args) > 1 {
        trial_num, _ = strconv.Atoi(os.Args[1])
    }

    shmem.Init()

    me := shmem.MyPe()
    npes := shmem.NPes()

    for i := 0; i < shmem.REDUCE_SYNC_SIZE; i++ {
        pSyncR[i] = shmem.SYNC_VALUE
    }

    // these are for use in collectives/reductions
    var pr *int64   = &pSyncR[0]
    var wr *int32   = &pWrkR[0]

    master := 0

    var a float64 = 1.0
    var b float64 = 1.0
    var l float64 = 1.0

    shmem.BarrierAll()

    if me == master {
        fmt.Printf("\n")
        fmt.Printf("BUFFON_LAPLACE - Master process:\n")
        fmt.Printf("  Rust version\n")
        fmt.Printf("\n")
        fmt.Printf("  A SHMEM example program to estimate PI\n")
        fmt.Printf("  using the Buffon-Laplace needle experiment.\n")
        fmt.Printf("  On a grid of cells of  width A and height B,\n")
        fmt.Printf("  a needle of length L is dropped at random.\n")
        fmt.Printf("  We count the number of times it crosses\n")
        fmt.Printf("  at least one grid line, and use this to estimate \n")
        fmt.Printf("  the value of PI.\n")
        fmt.Printf("\n")
        fmt.Printf("  The number of processes is %d\n", npes)
        fmt.Printf("\n")
        fmt.Printf("  Cell width A =    %.1f\n", a)
        fmt.Printf("  Cell height B =   %.1f\n", b)
        fmt.Printf("  Needle length L = %.1f\n", l)
        fmt.Printf("\n")
    }

    // remove rather pointless C++ output from here

    hit_num = buffon_laplace_simulate(a, b, l, int32(trial_num))

    shmem.BarrierAll()

    shmem.IntSumToAll(&hit_total, &hit_num, 1, 0, 0, npes, wr, pr)

    if me == master {
        trial_total := trial_num * npes

        pdf_estimate := float64(hit_total) / float64(trial_total)

        var pi_estimate float64 = 0.0

        if hit_total == 0 {
            pi_estimate = r8_huge()
        } else {
            pi_estimate = l * (2.0 * (a + b) - l) / (a * b * pdf_estimate)
        }

        pi_error := math.Abs( math.Pi - pi_estimate )

        fmt.Printf("\n")
        fmt.Printf("    %-8s  %-8s  %-8s  %-s16s  %-16s\n",
            "Trials",
            "Hits",
            "Estimated PDF",
            "Estimated Pi",
            "Error")
        fmt.Printf("\n")

        fmt.Printf("    %-8d  %-8d  %-16.5f  %-16.5f  %-16.5f\n",
            trial_total,
            hit_total,
            pdf_estimate,
            pi_estimate,
            pi_error)
    }

    if me == master {
        fmt.Printf("\n")
        fmt.Printf("BUFFON_LAPLACE - Master process:\n")
        fmt.Printf("Normal end of execution.\n")
    }

    shmem.Finalize()
}
