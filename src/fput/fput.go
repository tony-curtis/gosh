package main

import (
    "fmt"
    "bitbucket.org/tony-curtis/go-shmem" // OpenSHMEM namespace
)

var f1 float32 = 3.14
var f2 float32 = 0.0

func main() {
    shmem.Init()

    me := shmem.MyPe()

    if me == 0 {
        shmem.FloatPut(&f2, &f1, 1, 1)
    }

    shmem.BarrierAll()

    fmt.Printf("PE %4d: f1 = %.2f, f2 = %.2f\n", me, f1, f2)

    shmem.Finalize()
}
