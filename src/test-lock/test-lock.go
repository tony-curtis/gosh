package main

import (
    "fmt"
    "bitbucket.org/tony-curtis/go-shmem" // OpenSHMEM namespace
    "github.com/pborman/getopt/v2"
)

var count int = 0

var L shmem.Lock = shmem.LOCK_INIT

func main() {

    var (
        test bool = false
    )

    getopt.Flag(&test, 't', "use test instead of set")
    getopt.Parse()

    shmem.Init()

    me := shmem.MyPe()

    if test {
        if me == 0 { fmt.Printf("Using test_lock\n") }
        for {
            if ! shmem.TestLock(&L) {
                break
            }
        }
    } else {
        if me == 0 { fmt.Printf("Using set_lock\n") }
        shmem.SetLock(&L)
    }

    v := shmem.IntG(&count, 0)

    fmt.Printf("%d: count = %d\n", me, v)

    shmem.IntP(&count, v + 1, 0)

    shmem.ClearLock(&L)

    shmem.Finalize()
}
