package main

import (
    "fmt"
    "bitbucket.org/tony-curtis/go-shmem" // OpenSHMEM namespace
)

// symmetric
var dest int = 9

const to_add = 21

func main() {
    shmem.Init()

    me := shmem.MyPe()

    last_pe := shmem.NPes() - 1

    if me == 0 {
        shmem.IntAtomicAdd(&dest, to_add, last_pe)
    }

    shmem.BarrierAll()

    fmt.Printf("%-4d dest = %-4d\n", me, dest)

    shmem.Finalize()
}
