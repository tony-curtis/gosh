package main

import (
    "bitbucket.org/tony-curtis/go-shmem" // OpenSHMEM namespace
    "fmt"
)

const ONE_MB uint = 1024 * 1024

func main() {

    shmem.Init()

    var bytes uint = ONE_MB

    var area shmem.Memory = nil

    for true {
        report := bytes / ONE_MB

        area = shmem.Malloc(bytes)

        if area == nil {
            fmt.Printf("Failed at %d MB\n", report)
            break
        }

        shmem.Free(area)

        fmt.Printf("Alloc %d MB\n", report)

        bytes *= 2
    }

    shmem.Free(area)

    shmem.Finalize()

}
