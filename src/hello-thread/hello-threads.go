//
// Go interface to OpenSHMEM, demo with comments
//

package main

import (
    "fmt"
    "os"
    "strings"
    "bitbucket.org/tony-curtis/go-shmem" // OpenSHMEM namespace
)

const default_req = shmem.THREAD_MULTIPLE

func decode(tl int) string {
    switch tl {
    case shmem.THREAD_SINGLE:
        return "SHMEM_THREAD_SINGLE"
    case shmem.THREAD_FUNNELED:
        return "SHMEM_THREAD_FUNNELED"
    case shmem.THREAD_SERIALIZED:
        return "SHMEM_THREAD_SERIALIZED"
    case shmem.THREAD_MULTIPLE:
        return "SHMEM_THREAD_MULTIPLE"
    default:
        return "unknown"
    }
}
func encode(tl string) int {
    switch tl {
    case "SINGLE":
        return shmem.THREAD_SINGLE
    case "FUNNELED":
        return shmem.THREAD_FUNNELED
    case "SERIALIZED":
        return shmem.THREAD_SERIALIZED
    case "MULTIPLE":
        return shmem.THREAD_MULTIPLE
    default:
        return shmem.THREAD_SINGLE - 1
    }
}

func main() {

    var req int

    if len(os.Args) > 1 {
        req = encode(strings.ToUpper(os.Args[1]))
    } else {
        req = default_req
    }

    pro, s := shmem.InitThread(req)

    me   := shmem.MyPe()
    npes := shmem.NPes()

    host, _ := os.Hostname()

    see := shmem.QueryThread()

    if me == 0 {
        fmt.Printf("%s: PE %4d of %4d init returned status %d\n",
            host, me, npes, s)
        fmt.Printf("%s:   requested thread level %s (%d)\n",
            host, decode(req), req)
        fmt.Printf("%s:   thread init returned   %s (%d)\n",
            host, decode(pro), pro)
        fmt.Printf("%s:   queried thread level   %s (%d)\n",
            host, decode(see), see)
    }

    shmem.Finalize()

}
