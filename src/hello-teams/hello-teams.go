//
// Go interface to OpenSHMEM, demo with comments
//

package main

import (
    "fmt"
    "os"
    "bitbucket.org/tony-curtis/go-shmem" // OpenSHMEM namespace
)

func main() {

    shmem.Init()

    me    := shmem.TeamMyPe(shmem.TEAM_WORLD)
    npes  := shmem.TeamNPes(shmem.TEAM_WORLD)
    sme   := shmem.TeamMyPe(shmem.TEAM_SHARED)
    snpes := shmem.TeamNPes(shmem.TEAM_SHARED)

    host, _ := os.Hostname()

    fmt.Printf("%s: WORLD %d / %d SHARED %d / %d\n",
        host, me, npes, sme, snpes)

    shmem.BarrierAll()

    var c shmem.TeamConfig

    shmem.TeamGetConfig(shmem.TEAM_SHARED, &c)

    fmt.Printf("%s: SHARED team PE %d num_contexts %d\n",
        host, me, c.NumContexts)

    shmem.Finalize()

}
