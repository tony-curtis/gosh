package main

import (
    "bitbucket.org/tony-curtis/go-shmem" // OpenSHMEM namespace
    "fmt"
)

// symmetric
var counter int = 0

func main() {

    shmem.Init()

    me := shmem.MyPe()

    shmem.IntAtomicAdd(&counter, me + 1, 0)

    shmem.BarrierAll()

    if me == 0 {
        n := shmem.NPes()
        fmt.Printf("Sum from 1 to %d = %d\n", n, counter)
    }

    shmem.Finalize()

}
