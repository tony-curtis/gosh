//
// Go interface to OpenSHMEM, demo with comments
//

package main

import (
    "fmt"
    "os"
    "bitbucket.org/tony-curtis/go-shmem" // OpenSHMEM namespace
)

func main() {

    shmem.Init()

    me   := shmem.MyPe()
    npes := shmem.NPes()

    host, _ := os.Hostname()

    fmt.Printf("Hello from PE %4d of %4d on \"%s\"\n", me, npes, host)

    shmem.Finalize()

}
