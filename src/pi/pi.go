package main

import (
    "fmt"
    "math"
    "bitbucket.org/tony-curtis/go-shmem" // OpenSHMEM namespace
    "os"
    "strconv"
    "time"
)

const PI25DT float64 = 3.141592653589793238462643

/*
 * these all need to be symmetric as shmem targets
 */
var n int = 1000
var mypi float64
var pi float64

var pSyncB [shmem.BCAST_SYNC_SIZE]int64 /* for broadcast */
var pSyncR [shmem.REDUCE_SYNC_SIZE]int64 /* for reduction */
var pWrkR  [shmem.REDUCE_MIN_WRKDATA_SIZE]float64 /* for reduction */

func int64max(a int64, b int64) int64 {
    if a > b {
        return a
    } else {
        return b
    }
}

func f(a float64) float64 {
    return (4.0 / (1.0 + a * a))
}

func main() {

    if len(os.Args) > 1 {
        n, _ = strconv.Atoi(os.Args[1])
    }

    host, _ := os.Hostname()

    shmem.Init()

    numprocs := shmem.NPes()
    myid := shmem.MyPe()

    var start time.Time

    if myid == 0 {
        start = time.Now()
    }

    /* initialize sync arrays */
    for i := 0; i < shmem.BCAST_SYNC_SIZE; i++ {
        pSyncB[i] = shmem.SYNC_VALUE
    }
    for i := 0; i < shmem.REDUCE_SYNC_SIZE; i++ {
        pSyncR[i] = shmem.SYNC_VALUE
    }

    // these are for use in collectives/reductions
    var pb *int64   = &pSyncB[0]
    var pr *int64   = &pSyncR[0]
    var wr *float64 = &pWrkR[0]

    shmem.BarrierAll()

    /* -=- set up done -=- */

    /* send "n" out to everyone */
    np := shmem.Memory(&n)
    shmem.Broadcast32(np, np, 1, 0, 0, 0, numprocs, pb)

    /* do partial computation */
    h := 1.0 / (float64)(n)
    sum := 0.0
    /* A slightly better approach starts from large i and works back */
    for i := myid + 1; i <= n; i += numprocs {
        x := h * ((float64)(i) - 0.5)
        sum += f(x)
    }
    mypi = h * sum

    /* wait for everyone to finish */
    shmem.BarrierAll()

    /* add up partial pi computations into PI */
    shmem.DoubleSumToAll(&pi, &mypi, 1, 0, 0, numprocs, wr, pr)

    fmt.Printf("PE %d on node %s\n", myid, host)

    /* "master" PE summarizes */
    if myid == 0 {
        elapsed := time.Since(start)
        fmt.Printf(
            "pi is approximately %.16f, Error is %.16f\n",
            pi, math.Abs(pi - PI25DT))
        fmt.Println("Took: ", elapsed)
    }

    shmem.Finalize()
}
