package main

import (
    "fmt"
    "bitbucket.org/tony-curtis/go-shmem" // OpenSHMEM namespace
)

func main() {
    shmem.Init()

    // explicit type
    var p shmem.Memory = shmem.Malloc(64)

    // implicit type
    q := shmem.Calloc(64, 4)

    fmt.Println("p = ", p, "    q = ", q)

    shmem.Free(q)
    shmem.Free(p)

    shmem.Finalize()
}
