package main

import (
    "fmt"
    "bitbucket.org/tony-curtis/go-shmem" // OpenSHMEM namespace
)

var race_winner int = -1

func main() {
    shmem.Init()

    me := shmem.MyPe()

    oldval := shmem.IntAtomicCompareSwap(&race_winner, -1, me, 0)

    if oldval == -1 {
        fmt.Printf("PE %d was first\n", me)
    }

    shmem.Finalize()
}
