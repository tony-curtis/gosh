module bitbucket.org/tony-curtis/gosh

go 1.16

require (
	bitbucket.org/tony-curtis/go-shmem v0.1.7
	github.com/pborman/getopt/v2 v2.1.0
)
